
var express = require('express');
var app = express();
fibrous = require('fibrous');
url = require("url");
captcha = require("CaptchaParser");
fs = require("fs");
unirest = require("unirest");

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

// set the view engine to ejs
app.set('view engine', 'ejs');

// make express look in the public directory for assets (css/js/img)
app.use(express.static(__dirname + '/public'));
app.use(fibrous.middleware);

// set the home page route
app.get('/', function(req, res) {

    console.log("I got kicked");
	var captchaUri = 'https://academics.vit.ac.in/parent/captcha.asp';

	var _get = url.parse(req.url, true).query;
	res.write('Welcome on Main Page ' + _get['cookie_key'] + " -> "+ _get['cookie_val'] + "\n");
	var cookieString = _get['cookie_key']+'='+_get['cookie_val'] + ";";

	var CookieJar = unirest.jar();
	CookieJar.add(unirest.cookie(_get['cookie_key']+'='+_get['cookie_val'])); // Cookie string, pathname / url

	var req_headers={ "user-agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,/*;q=0.8",
					"Accept-Encoding": "gzip, deflate",
					"Accept-Language": "en-US,en;q=0.7,mr;q=0.3",
                    "Connection": "keep-alive",
                    "DNT": "1",
					"Referer": "https://academics.vit.ac.in/student/stud_login.asp"
//			        "Cookie": cookieString
				};


	unirest.get(captchaUri)
//			.jar(CookieJar)
			.headers(req_headers)
			.encoding(null)
			.set('Content-Type', 'image/bmp')
			.end(function (aca_response) {
					if (aca_response.error) {
						console.log('VIT Academics connection failed');
					}
					else {
					    var pixMap = captcha.getPixelMapFromBuffer(aca_response.body);
						var val = captcha.getCaptcha(pixMap);
//						console.log("this" + aca_response.headers('Cookie'));
						console.log("this" + aca_response.cookies);
						res.write("Yoing -> <h2 id='cap_val'>" + val + "</h2>");
						res.end();
					}
				});

});

app.use (function(req, res, next) {
  console.log("statrt");	
    var data=[];
    req.on('data', function(chunk) { 
       data.push(chunk);
    });
    req.on('end', function() {
  		console.log("end");	
      req.body = Buffer.concat(data);
		  next();
    });
});

var DoneInSync = fibrous(function(buffer){
	var pixMap = captcha.getPixelMapFromBuffer(buffer);
	var val = captcha.getCaptcha(pixMap);
	console.log("this" + val);	
	return val;
});

app.post('/', function (req,res){
	
 	fs.writeFileSync("captchas_ass.bmp", req.body);
	var val = DoneInSync.sync(req.body);
  res.write("Captcha Value->" + val);
  res.end();

});


app.listen(port, function() {
    console.log('Our app is running on http://localhost:' + port);
});



/*
function rawBody(req, res, next) {	
	console.log("called yaya");
    var chunks = [];
    req.on('data', function(chunk) {
		console.log("Chunk");
        chunks.push(chunk);
    });
    req.on('end', function() {
        var buffer = Buffer.concat(chunks);
        req.bodyLength = buffer.length;
        req.rawBody = buffer;
        next();
    });
    req.on('error', function (err) {
        console.log(err);
        res.sendStatus(500);
		console.log("plz do it now");
    });
}
*/


/*
NOT Working post 

	if (req.rawBody && req.bodyLength > 0) {
	    pixMap = captcha.getPixelMapFromBuffer(req.rawBody);
		fs.writeFileSync("captchas_ass.bmp", req.rawBody);
		val = captcha.getCaptcha(pixMap);
		console.log("this " + val);
		res.write(" -> " + val);							
    } else {
        res.sendStatus(500);
    }
	res.end();

/*
	res.write('Welcome on Main Page ' + "I got an image");
	if (req.error) {
		console.log('Server failed you');
	}
	else {
		data come in "req.file" regardless of the attribute "name". 
		var tmp_path = req.file.path;

		var target_path = 'uploads/' + req.file.originalname;
		console.log(tmp_path);
		console.log(target_path);
		console.log(req.file);
		pixMap = captcha.getPixelMapFromBuffer(req.file.buffer);
		fs.writeFileSync("captcha.bmp", aca_response.body);
		var val = captcha.getCaptcha(pixMap);
		console.log("this" + val);
		res.write(" -> "+val);							
		res.end();
	}
*/


/*

Not working GET 

	var cookieString = _get['cookie_key']+'='+_get['cookie_val'];	
	var options = {
		hostname: "academics.vit.ac.in",
		path: '/student/captcha.asp',
		method: 'GET',
		headers: { "user-agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,/*;q=0.8",
					"Accept-Encoding": "gzip, deflate",
					"Accept-Language": "en-US,en;q=0.7,mr;q=0.3",
                    "Connection": "keep-alive",
                    "DNT": "1",
					"Referer": "https://academics.vit.ac.in/student/stud_login.asp",
			        "Cookie": cookieString
				}
	};

	https.request(options, function (resp) {
		data = [];
		resp.setEncoding('binary');
		//console.log(resp.headers);
		if (resp.statusCode) {
		    resp.on('data', function (chunk) {
				console.log("addes chunk");
				data.push(chunk);
		    });
		    resp.on('end', function (part) {
				console.log('File saved.');
				buffer = Buffer.concat(data);
			    pixMap = captcha.getPixelMapFromBuffer(buffer);
				val = captcha.getCaptcha(pixMap);
				console.log("this" + val);				
				res.write(" -> " + val);
				res.end();
		    });
		    resp.on('error', function (e) {
		        console.log('Problem with request: ' + e.message);
		    });
		}
	}).end();

*/

